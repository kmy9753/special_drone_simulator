﻿using UnityEngine;
using System.Collections;

public class ViewChanger : MonoBehaviour {

	private GameObject _camera;
	[SerializeField] private Transform nextCamera;

	// Use this for initialization
	void Start () {
	
		_camera = Camera.main.gameObject;
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void ChangeView () {
		
		iTween.MoveTo(_camera, iTween.Hash(
			"position", nextCamera,
			"time", 0.9f, 
			"oncomplete", "complete", 
			"oncompletetarget", this.gameObject, 
			"easeType", "linear"
			));
		
		iTween.RotateTo(_camera, iTween.Hash(
			"rotation", nextCamera,
			"time", 0.9f
			));
		
	}
	
}
