﻿using UnityEngine;
using System.Collections;

public class PlayerControllerWithCursor : MonoBehaviour {

	[SerializeField] private float speed;
	
	// Update is called once per frame
	void Update () {
	
		Vector3 position = this.transform.position;
		
		if (Input.GetKey(KeyCode.LeftArrow) || Input.GetKey(KeyCode.A)){
			position.x -= this.speed;
		}
		if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.D)){
			position.x += this.speed;
		}
		if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W)){
			position.z += this.speed;
		}
		if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S)){
			position.z -= this.speed;
		}
		
		this.transform.position = position;
		
	}

}
