﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

using Game;

namespace DualDecomposition {

	public class CenterScript : MonoBehaviour {
	
		[SerializeField] private CircleMasterSystem cMasterSystem;
		[SerializeField] private TigerMasterSystem  tMasterSystem;
		[SerializeField] private KamiyaMasterSystem kMasterSystem;
		[SerializeField] private Edge1MasterSystem  e1MasterSystem;
		[SerializeField] private PincerMasterSystem  pMasterSystem;

		[SerializeField] private GameObject drones;
		[SerializeField] private Transform target;

		private float intervalTime;
		private float ColliderRadius;
		private bool active;

		private bool isNearTarget;

		// [maybe later] id is kuso
		private int invincibleID;
		private Queue<int> queue_invincibleID;

		[SerializeField] private CapsuleCollider CapCen;

		private bool edgeflag = false;
		private bool canCountAvoidance = true;

		void Start () {

			this.tMasterSystem.UnsetActive ();
			this.cMasterSystem.UnsetActive ();
			this.kMasterSystem.UnsetActive ();
			this.e1MasterSystem.UnsetActive ();
			this.pMasterSystem.UnsetActive ();

			this.active = false;
			this.isNearTarget = false;

			this.invincibleID = 0;
			queue_invincibleID = new Queue<int> ();
		}

		public void Activate () {

			this.SwitchFormation (0);
			this.drones.SetActive (true);
			this.active = true;

		}

		// Update is called once per frame
		void Update () {

			if (!active) return;
			this.intervalTime += Time.deltaTime;

			if (this.intervalTime >= 1.5f) {
				if (this.cMasterSystem.IsActive ()) {
					this.cMasterSystem.DecreaseRadiusStar ();
					ColliderRadius = this.cMasterSystem.GetRadiusStar() /3f;
				}
				if (this.kMasterSystem.IsActive ()) {
					this.kMasterSystem.DecreaseRadiusStar ();
					ColliderRadius = this.kMasterSystem.GetRadiusStar() /3f;
				}
				if (this.e1MasterSystem.IsActive ()) {
					this.e1MasterSystem.DecreaseRadiusStar ();
					ColliderRadius = this.e1MasterSystem.GetRadiusStar() /7f;
				}
				if (this.pMasterSystem.IsActive ()) {
					this.pMasterSystem.DecreaseRadiusStar ();
					ColliderRadius = this.pMasterSystem.GetRadiusStar() /3f;
				}
				if (this.tMasterSystem.IsActive ()) {
					if (!this.isNearTarget) {
						iTween.MoveTo (this.gameObject, iTween.Hash (
							"position", target.position,
							"time", 8f,
							"oncomplete", "OnComplete", 
							"oncompletetarget", this.gameObject, 
							"easeType", iTween.EaseType.easeOutSine
						));
					}
					ColliderRadius = 4f;
				}
				if(CapCen != null){
					CapCen.radius = ColliderRadius;
				}

				this.intervalTime = 0f;
			}
			
		}

        void UpdateMissileFormation () {

            iTween.MoveTo (this.gameObject, iTween.Hash (
                           "position", target.position,
                           "time", 4f,
                           "oncomplete", "OnComplete", 
                           "oncompletetarget", this.gameObject, 
                           "easeType", iTween.EaseType.easeOutSine
            ));

        }
		
		void OnTriggerEnter (Collider collider) {

			if (!active) return;
					
			if(collider.transform.tag == "Player" && !this.edgeflag) {
				int rand = Random.Range (0, 5);
				this.isNearTarget = true;

				if (rand == 0 || rand == 4) {
                    InvokeRepeating ("UpdateMissileFormation", 0f, 4f);
					return;
				}

				this.SwitchFormation (rand);

				iTween.MoveTo (this.gameObject, iTween.Hash (
					"x", collider.transform.position.x,
					"z", collider.transform.position.z,
					"time", 2f,
					"oncomplete", "OnComplete", 
					"oncompletetarget", this.gameObject, 
					"easeType", iTween.EaseType.easeOutSine
				));
				
				this.intervalTime = 0f;
				this.cMasterSystem.SetRadiusStar(10f);
				this.kMasterSystem.SetRadiusStar(10f);
				
			}
		
		}

		private void InvincibleMode(){
			// todo: it should be without invincible mode
			foreach (Transform child in this.drones.transform) {
				child.GetComponent<Collider> ().enabled = false;
			}

			this.invincibleID++;
			this.queue_invincibleID.Enqueue (this.invincibleID);
			Invoke ("FinishInvincibleMode", 1.5f);
		}

		void FinishInvincibleMode () {

			int id = this.queue_invincibleID.Dequeue ();

			if (id == this.invincibleID) {
				this.SetColliderToDrones ();
			}

		}

		// todo: it should be without invincible mode
		void SetColliderToDrones () {

			foreach (Transform child in this.drones.transform) {
				child.GetComponent<Collider> ().enabled = true;
			}

		}
		
		void OnTriggerExit (Collider collider) {

			if (!active) return;
			this.isNearTarget = false;
		
			if(collider.transform.tag == "Player" && edgeflag == false ) {

				this.SwitchFormation(0);

			}

			this.CountAvoidance ();
		
		}
		
		public void EdgeOnTriggerEnter (int Status,Vector3 CornerPos){

			if (!active) return;

			edgeflag = true;
			this.e1MasterSystem.SetStatus (Status);
			this.e1MasterSystem.SetRadiusStar(20f);
			this.SwitchFormation (-1);

			iTween.MoveTo(this.gameObject, iTween.Hash(
				"position", CornerPos,
				"time", 4f,
				"oncomplete", "OnComplete", 
				"oncompletetarget", this.gameObject, 
				"easeType", iTween.EaseType.easeOutSine
			));

		}

		public void EdgeOnTriggerExit (){

			if (!active) return;

			edgeflag = false;
			this.SwitchFormation (0);

			this.CountAvoidance ();

		}

		void CountAvoidance () {

			if (!this.canCountAvoidance)
				return;
			
			GameDataHolder.Instance.num_avoidance++;
			EffectRenderer.Instance.MakeNiceAvoidanceEffect (this.transform.position);

			this.canCountAvoidance = false;
			Invoke ("SetCanCountAvoidance", 2f);

		}

		void SetCanCountAvoidance () {

			this.canCountAvoidance = true;

		}

		private void SwitchFormation (int form) {

            CancelInvoke ("UpdateMissileFormation");

			this.tMasterSystem.UnsetActive();
			this.cMasterSystem.UnsetActive();
			this.kMasterSystem.UnsetActive();
			this.e1MasterSystem.UnsetActive();
			this.pMasterSystem.UnsetActive();

			switch(form){
			case 0:
			case 4:
				this.tMasterSystem.SetActive();
				break;
			case 1:
				this.cMasterSystem.SetActive();
				break;
			case 2:
				this.kMasterSystem.SetActive();
				break;
			case 3:
				this.pMasterSystem.SetActive();
				break;
			default:
				this.e1MasterSystem.SetActive();
				break;
			}
			InvincibleMode ();
		}
		
	}

}
