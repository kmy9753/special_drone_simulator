﻿using UnityEngine;
using System.Collections;

public class RotateToPlayerWithAMaximumSpeed : MonoBehaviour {
	
	public float maximumRotateSpeed = 40;
	public float minimumTimeToReachTarget = 0.5f;
	[SerializeField] private Transform playerTransform;
	private float velocity;

	// Update is called once per frame
	void Update () {
		
		var newRotation = Quaternion.LookRotation(this.playerTransform.position - this.transform.position).eulerAngles;
		var angles = this.transform.rotation.eulerAngles;
		this.transform.rotation = 
			Quaternion.Euler(angles.x,
							 Mathf.SmoothDampAngle(angles.y, newRotation.y, ref velocity, minimumTimeToReachTarget, maximumRotateSpeed),
		                     angles.z);
		
	}
}
