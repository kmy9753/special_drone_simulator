# どきどき！鬼ごっこ

近場の公園で一人鬼ごっこをしよう！  
さまざまなフォーメーションを組みながら追ってくるドローンたちの攻撃をかわせ！  


備考
------
#### 動作確認端末
- Android 4.4.2
- Android 5.0

#### 推奨環境
- 15m四方以上
- 周囲に建物が少ない
- フィールド内を人が通らない


プレイ画面
--------

|||
|---|---|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/onigokko/howtoplay_title.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/onigokko/howtoplay_game_preset_norun.jpg)|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/onigokko/howtoplay_game_preset_run.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/onigokko/howtoplay_game.jpg)|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/onigokko/ss_game.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/onigokko/ss_result1.jpg)|