﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using DG.Tweening;

namespace HowtoPlay {
	public class EffectRenderer : SingletonMonoBehaviour<EffectRenderer> {

		[SerializeField] RectTransform allImagesObj;
		[SerializeField] int max_moveCnt;

		// kuso
		[SerializeField] Transform leftTransform;
		[SerializeField] Transform rightTransform;
		private float diff_x;

		int moveCnt = 0;
		bool isMoving = false;

		// Use this for initialization
		void Start () {

			this.diff_x = this.rightTransform.position.x - this.leftTransform.position.x;

		}

		// Update is called once per frame
		void Update () {

		}

		public void MoveToLeft () {

			if (this.moveCnt == 0 || this.isMoving)
				return;

			this.moveCnt--;
			this.isMoving = true;

			allImagesObj.DOMove(this.allImagesObj.position + new Vector3(this.diff_x, 0, 0), 1.0f).OnComplete(CompleteMovement);

		}

		public void MoveToRight () {

			if (this.moveCnt == this.max_moveCnt || this.isMoving)
				return;

			this.moveCnt++;
			this.isMoving = true;

			allImagesObj.DOMove(this.allImagesObj.position + new Vector3(-this.diff_x, 0, 0), 1.0f).OnComplete(CompleteMovement);

		}

		void CompleteMovement () {

			this.isMoving = false;

		}

	}
}
