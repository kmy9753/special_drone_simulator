﻿using UnityEngine;
using System.Collections;

namespace Title {
	public class EffectRenderer : SingletonMonoBehaviour<EffectRenderer> {

		[SerializeField] GameObject titleObj;

		// Use this for initialization
		void Start () {
			this.FadeInTitleObj ();
		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void FadeOutTitleObj () {

			iTween.MoveBy(this.titleObj, iTween.Hash("y", -1200f, "time", 1.3f, "easetype", iTween.EaseType.easeOutCirc));

		}

		public void FadeInTitleObj () {

			this.titleObj.transform.Translate(new Vector3(0f, -100f, 0f));
			iTween.MoveBy(this.titleObj, iTween.Hash("y", 100f, "time", 1f, "easetype", iTween.EaseType.easeOutBounce));

		}
			
	}
}
