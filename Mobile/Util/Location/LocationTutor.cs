﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class LocationTutor : MonoBehaviour {

	[SerializeField] private GameObject distination_obj;
	[SerializeField] private Transform distinationLowerLeft;
	[SerializeField] private Transform distinationUpperLeft;
	[SerializeField] private Transform distinationUpperRight;
	[SerializeField] private Transform distinationCenter;

	[SerializeField] private GameObject tutorPanel;
	[SerializeField] private Text tutorText;
	[SerializeField] private string tutorTextLowerLeft;
	[SerializeField] private string tutorTextUpperLeft;
	[SerializeField] private string tutorTextUpperRight;
	[SerializeField] private string tutorTextCenter;
	[SerializeField] private string tutorTextDetermination;
	
	[SerializeField] float sensingBorderTime;
	[SerializeField] int maxLocationData;

	[SerializeField] private PlayerControllerWithGPS playerController;
	[SerializeField] private PlayerControllerWithSensors playerController_sensors;
	
	private _Vector2	sumLocationData; // = (longitude, latitude)
	// private float       sumAngleData = 0f;
	private int numLocationData = 0;
	private GPSManager gpsManager;
	private bool activeDetermination = false;

	enum Corners { LowerLeft, UpperLeft, UpperRight, Center };
	private Corners cur_corner;

	// Use this for initialization
	void Start () {

		this.gpsManager = GPSManager.Instance;
		this.cur_corner = Corners.LowerLeft;

		if (ConfigMaster.Instance.UseSavedLocation () || ConfigMaster.Instance.PlayWithoutGPS ()) {
			// this.distination_obj.transform.position = this.distinationCenter.position;
			// this.CompleteObjMovementToCenter ();
			iTween.MoveTo(this.distination_obj, iTween.Hash(
				"position", this.distinationCenter,
				"time", 0.9f, 
				"oncomplete", "CompleteObjMovementToCenter",
				"oncompletetarget", this.gameObject, 
				"easeType", iTween.EaseType.easeInOutSine
			));
		} else {
			this.distination_obj.transform.position = this.distinationLowerLeft.position;
			this.tutorText.text = this.tutorTextLowerLeft;
		}

	}
	
	private void AddLocation () {

		gpsManager.SensingLocation();
		this.sumLocationData += this.gpsManager.GetLocation();

		/*
		if (cur_corner == Corners.LowerLeft) {
			this.sumAngleData += OrientationManager.Instance.GetOrientation ();
		}
		*/
	
		this.numLocationData++;
		if(this.numLocationData >= this.maxLocationData) {
			CancelInvoke("AddLocation");
			
			this.DetermineLocation();
		}

	}

	// runs summation process =assign=> this.sumLocationData
	public void InputLocation () {

		if(!this.gpsManager.HasCorrectLocation () || this.activeDetermination || this.cur_corner == Corners.Center) {
			return;
		}

		this.activeDetermination = true;
		this.tutorText.text = this.tutorTextDetermination;

		this.sumLocationData = new _Vector2 (0, 0);
		this.numLocationData = 0;

		InvokeRepeating("AddLocation", this.sensingBorderTime, this.sensingBorderTime);
		
	}

	// maybe later: make each operations composite
	void DetermineLocation () {
	
		_Vector2 location = this.sumLocationData / this.maxLocationData;
		this.tutorText.text = "";
	
		switch(this.cur_corner) {
		case Corners.LowerLeft:
			LocationHolder.Instance.SetLocationLowerLeft (location);

			iTween.MoveTo (this.distination_obj, iTween.Hash (
				"position", this.distinationUpperLeft,
				"time", 0.9f, 
				"oncomplete", "CompleteObjMovementToUpperLeft",
				"oncompletetarget", this.gameObject, 
				"easeType", iTween.EaseType.easeInOutSine
			));

			// LocationHolder.Instance.SetVerticalAngle_deg (sumAngleData / this.maxLocationData);
			break;

		case Corners.UpperLeft:
			LocationHolder.Instance.SetLocationUpperLeft (location);
		
			iTween.MoveTo(this.distination_obj, iTween.Hash(
				"position", this.distinationUpperRight,
				"time", 0.9f, 
				"oncomplete", "CompleteObjMovementToUpperRight",
				"oncompletetarget", this.gameObject, 
				"easeType", iTween.EaseType.easeInOutSine
				));
			break;
				
		case Corners.UpperRight:
			LocationHolder.Instance.SetLocationUpperRight (location);
		
			iTween.MoveTo(this.distination_obj, iTween.Hash(
				"position", this.distinationCenter,
				"time", 0.9f, 
				"oncomplete", "CompleteObjMovementToCenter",
				"oncompletetarget", this.gameObject, 
				"easeType", iTween.EaseType.easeInOutSine
				));
			break;
		}
	
	}
	
	void CompleteObjMovementToUpperLeft () {
	
		this.cur_corner = Corners.UpperLeft;
		this.tutorText.text = this.tutorTextUpperLeft;
		this.activeDetermination = false;

	}
	void CompleteObjMovementToUpperRight () {
		
		this.cur_corner = Corners.UpperRight;
		this.tutorText.text = this.tutorTextUpperRight;
		this.activeDetermination = false;
		
	}
	
	void CompleteObjMovementToCenter () {
	
		this.cur_corner = Corners.Center;
		this.tutorText.text = this.tutorTextCenter;
		this.activeDetermination = false;

		this.playerController.Activate (this.distinationUpperRight);

		if (!ConfigMaster.Instance.PlayWithoutGPS ()) {
			this.playerController_sensors.Activate ();
		}
		
	}

	public void DestroyObj () {

		Destroy (this.tutorPanel);
		Destroy (this.gameObject);

	}

}
