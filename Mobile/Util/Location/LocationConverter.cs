﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI; // unit test

public class LocationConverter : SingletonMonoBehaviour<LocationConverter> {

	public Transform floor;

	// in the real world
	private _Vector2 p0;     // location of LowerLeft
	private _Vector2 p1;     // location of UpperLeft
	private _Vector2 p2;     // location of UpperRight
	private _Vector2 p3;     // location of LowerRight

	private double a_hol; // slope on a horizontal graph
	private double a_ver; // slope on a vertical   graph

	/* unit test (verify calculation(real loc. -> virtual pos.)) (1/2)
	[SerializeField] private _Vector2 p0;     // location of LowerLeft
	[SerializeField] private _Vector2 p1;     // location of UpperLeft
	[SerializeField] private _Vector2 p2;     // location of UpperRight
	private _Vector2 p3;     // location of LowerRight

	[SerializeField] private Vector2 pp;
	//*/

	//* unit test (view location(p0, p1, p2)) (1/2)
	[SerializeField] Text text_ll;
	[SerializeField] Text text_ul;
	[SerializeField] Text text_ur;
	//*/

	//* unit test (view real scale) (1/2)
	[SerializeField] Text text_realScale;
	//*/

	// in the virtual world
	private Vector2 origin; // point of O
	private float height;
	private float width;

	private bool preCalculated_ConvertToVirtual;

	private Vector2 realScale;
	private bool preCalculated_GetRealScale;

	// Use this for initialization
	void Start () {
		
		this.preCalculated_ConvertToVirtual = false;
		this.preCalculated_GetRealScale     = false;

	}

/*
	// unit test (verify calculation(real loc. -> virtual pos.)) (2/2)
	public void OnTestCalculation () {

		this.preCalculated = false;
		print (this.ConvertToVirtual (pp));

	}
*/

	public Vector2 ConvertToVirtual (_Vector2 p) {

		if (!this.preCalculated_ConvertToVirtual) {
			this.p0 = LocationHolder.Instance.GetLocationLowerLeft  ();
			this.p1 = LocationHolder.Instance.GetLocationUpperLeft  ();
			this.p2 = LocationHolder.Instance.GetLocationUpperRight ();
			this.p3 = p2 + (p0 - p1);

			this.a_hol = (p3.y - p0.y) / (p3.x - p0.x);
			this.a_ver = (p1.y - p0.y) / (p1.x - p0.x);

			this.width  = floor.lossyScale.x;
			this.height = floor.lossyScale.z;
			this.origin = new Vector2 (floor.position.x - this.width  / 2f,
									   floor.position.z - this.height / 2f);

			this.preCalculated_ConvertToVirtual = true;

			//* unit test (view location(p0, p1, p2)) (2/2)
			this.text_ll.text = p0.x.ToString("F7") + ", " + p0.y.ToString("F7");
			this.text_ul.text = p1.x.ToString("F7") + ", " + p1.y.ToString("F7");
			this.text_ur.text = p2.x.ToString("F7") + ", " + p2.y.ToString("F7");
			//*/
		}

		double t = (p.y - a_hol * p.x - p0.y + a_hol * p0.x) / ((p1.y - a_hol * p1.x) - (p0.y - a_hol * p0.x));
		double s = (p.y - a_ver * p.x - p0.y + a_ver * p0.x) / ((p3.y - a_ver * p3.x) - (p0.y - a_ver * p0.x));

		return this.origin + new Vector2 ((float)s * this.width, (float)t * this.height);

	}

	public Vector2 GetRealScale () {

		if (!this.preCalculated_ConvertToVirtual)
			this.ConvertToVirtual (p0);

		if (!this.preCalculated_GetRealScale) {
			_Vector2 r = new _Vector2 (6378137.000, 6356752.314245);

			this.realScale = new Vector2 (this.CalculateRealDistance_m(p1, p2, r),
										  this.CalculateRealDistance_m(p0, p1, r));

			this.preCalculated_GetRealScale = true;
		}

		//* unit test (view real scale) (2/2)
		this.text_realScale.text = this.realScale.ToString("F3");
		//*/

		return this.realScale;

	}

	public float CalculateRealDistance_m (_Vector2 a, _Vector2 b, _Vector2 R) {

		// TODO: debug calculation
		return 17f;

		_Vector2 D = new _Vector2 (a.x - b.x, a.y - b.y);

		float P_rad = (float)(((a.y + b.y) / 2.0) / 180.0 * Mathf.PI);

		// float E = Mathf.Sqrt ((float)((R.x * R.x - R.y * R.y) / (R.x * R.x)));
		float E2 = 0.00669437999019758f;
		// float W = Mathf.Sqrt (1f - E * E * Mathf.Pow(Mathf.Sin(P_rad), 2f));
		float W = Mathf.Sqrt (1f - E2 * Mathf.Pow(Mathf.Sin(P_rad), 2f));

		// float M = (float)(R.x * (1f - E * E) / Mathf.Pow(W, 3f));
		float M = (float) (6335439.32729246 / W / W / W);
		float N = (float)(R.x / W);

		float dist = Mathf.Sqrt (Mathf.Pow((float)(D.y * M)                   , 2f)
							   + Mathf.Pow((float)(D.x * N * Mathf.Cos(P_rad)), 2f));

		return dist;

	}

}
