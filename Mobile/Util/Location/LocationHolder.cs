﻿using UnityEngine;
using System.Collections;

public class LocationHolder : SingletonMonoBehaviour<LocationHolder> {

	private _Vector2 loc_lowerLeft;
	private _Vector2 loc_upperLeft;
	private _Vector2 loc_upperRight;

	void Awake ()
	{
		if (this != Instance) {
			Destroy (this);
			return;
		}

		DontDestroyOnLoad (this.gameObject);

	}

	public void SetLocationLowerLeft (_Vector2 location) {

		this.loc_lowerLeft = location;

	}
	public void SetLocationUpperLeft (_Vector2 location) {

		this.loc_upperLeft = location;

	}
	public void SetLocationUpperRight (_Vector2 location) {

		this.loc_upperRight = location;

	}

	public _Vector2 GetLocationLowerLeft () {

		return this.loc_lowerLeft;

	}
	public _Vector2 GetLocationUpperLeft () {

		return this.loc_upperLeft;

	}
	public _Vector2 GetLocationUpperRight () {

		return this.loc_upperRight;

	}

}
