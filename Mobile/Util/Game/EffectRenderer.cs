﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Game {
	public class EffectRenderer : SingletonMonoBehaviour<EffectRenderer> {

		private GameObject cur_fadeoutObj;
		private GameObject cur_intoViewObj;
		private Queue<GameObject> bounceViewObj;

		[SerializeField] private GameObject startTextField;
		[SerializeField] private GameObject clearTextField;
		[SerializeField] private GameObject gameoverTextField;

		[SerializeField] private GameObject niceAvoidTextField;
		[SerializeField] private GameObject touchEffect;

		// Use this for initialization
		void Start () {
		
			this.bounceViewObj = new Queue<GameObject> ();

		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void MakeGameStartEffect () {

			this.StartIntoView (this.startTextField);

		}

		public void MakeClearEffect () {

			this.StartIntoView (this.clearTextField);

		}

		public void MakeGameoverEffect () {

			this.StartIntoView (this.gameoverTextField);

		}

		// WARNING: you don't call this function for SOME objects
		void StartIntoView (GameObject obj) {

			iTween.MoveBy(obj, iTween.Hash("x", -50f, "time", 1f, "easetype", iTween.EaseType.easeOutBounce,
				"oncomplete", "CompleteIntoView", "oncompletetarget", this.gameObject));
			this.cur_intoViewObj = obj;

		}

		void CompleteIntoView () {

			iTween.MoveBy(this.cur_intoViewObj, iTween.Hash("x", -60f, "time", 1f, "easetype", iTween.EaseType.linear));

		}

		// WARNING: you don't call this function for SOME objects
		public void FadeoutAndDestroy (GameObject obj) {

			iTween.FadeTo(obj, iTween.Hash ("a", 0, "time", 2.0f, "oncompletetarget", this.gameObject, "oncomplete", "CompleteFadeout"));
			this.cur_fadeoutObj = obj;

		}

		void CompleteFadeout () {

			Destroy (this.cur_fadeoutObj);

		}

		public void MakeNiceAvoidanceEffect (Vector3 point) {

			GameObject cur_bounceViewObj = (GameObject)Instantiate (this.niceAvoidTextField, new Vector3 (point.x, -20f, point.z), Quaternion.Euler(new Vector3(90f, 354f, 0f)));
			this.bounceViewObj.Enqueue (cur_bounceViewObj);

			// iTween.MoveTo (this.cur_bounceViewObj,);
			iTween.MoveTo (cur_bounceViewObj, iTween.Hash (
				"position", new Vector3(point.x, 2f, point.z),
				"time", 0.8f, 
				"oncomplete", "CompleteBounceView",
				"oncompletetarget", this.gameObject, 
				"easeType", iTween.EaseType.easeOutBounce
			));

		}

		public void MakeTouchEffect (Vector3 point) {

			Instantiate (this.touchEffect, point, Quaternion.identity);

		}

		void CompleteBounceView () {

			Destroy (this.bounceViewObj.Dequeue ());

		}

	}
}
