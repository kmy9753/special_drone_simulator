﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Result {
	public class ResultTextManager : MonoBehaviour {

		[SerializeField] Text [] textFields;
		private int cur_idx;

		private enum TextType { RTime = 0, NAvoidance, Calory, Result };

		// Use this for initialization
		void Start () {

			this.cur_idx = 0;
			Invoke ("ShowText", 1.5f);

		}

		void ShowText () {

			if (this.cur_idx >= this.textFields.Length)
				return;

			string val_str = "";
				switch((TextType)this.cur_idx){
			case TextType.RTime:
					val_str = (ConfigMaster.Instance.GetTimeLimit () - GameDataHolder.Instance.remainingTime).ToString() + "秒";
					break;
				case TextType.NAvoidance:
					val_str = GameDataHolder.Instance.num_avoidance.ToString () + "回";
					break;
				case TextType.Calory:
					val_str = "10kcal";
					break;
				case TextType.Result:
					if (GameDataHolder.Instance.remainingTime == 0) {
						val_str = "完走！";
						EffectRenderer.Instance.MakeClearEffect ();
					}
					else {
						val_str = "完走ならず";
						EffectRenderer.Instance.MakeGameOverEffect ();
					}
					
					break;
			}
			this.textFields [this.cur_idx].text += val_str;

			this.cur_idx++;
			Invoke ("ShowText", 1f);

		}

	}
}
