﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

using DG.Tweening;

namespace Result {
	public class VoiceTextManager : MonoBehaviour {

		[SerializeField] private string[] speechTexts;
		[SerializeField] private Text textField;
		[SerializeField] private GameObject targetObject;

		[SerializeField] private Animator animator;

		public bool isShow;
		private bool isHiding;
		private Vector3 delta;
		private Vector3 origin_targetLocalPosition;

		// Use this for initialization
		void Start () {

			this.delta = new Vector3 (80f, -40f, 0f);

			this.textField.text = this.speechTexts[Random.Range(0, this.speechTexts.Length)];

			this.origin_targetLocalPosition = this.targetObject.GetComponent<RectTransform> ().localPosition;
			this.isHiding = false;

			this.animator.SetBool ("Speak", true);

		}
		
		public void SwitchState () {

			if (this.isHiding)
				return;

			if (this.isShow) {
				this.HideText ();
			}

			else {
				this.ShowText ();
			}

			// this.animator.SetBool ("Speak", this.isShow);

		}

		private void ShowText () {

			this.targetObject.SetActive (true);
			RectTransform targetTransform = this.targetObject.GetComponent<RectTransform> ();

			targetTransform.localScale *= 0.2f;
			targetTransform.DOScale (new Vector3 (1f, 1f, 1f), 1f);

			targetTransform.localPosition += this.delta;
			targetTransform.DOBlendableLocalMoveBy (-delta, 1f);

			this.isShow = true;

		}

		private void HideText () {

			this.isHiding = true;

			RectTransform targetTransform = this.targetObject.GetComponent<RectTransform> ();

			targetTransform.DOScale (new Vector3 (0.2f, 0.2f, 0.2f), 1f).OnComplete(this.CompleteHide);;
			targetTransform.DOBlendableLocalMoveBy (this.delta, 1f).OnComplete(this.CompleteHide);

			this.isShow = false;

		}

		void CompleteHide () {

			this.targetObject.SetActive (false);
			this.targetObject.GetComponent<RectTransform> ().localPosition = this.origin_targetLocalPosition;

			this.isHiding = false;

		}
	}
}
