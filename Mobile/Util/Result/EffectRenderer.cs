﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Result {
	public class EffectRenderer : SingletonMonoBehaviour<EffectRenderer> {

		[SerializeField] GameObject clearEffect;
		[SerializeField] GameObject gameoverEffect;

		// Use this for initialization
		void Start () {

		}
		
		// Update is called once per frame
		void Update () {
		
		}

		public void MakeClearEffect () {

			Instantiate (this.clearEffect);

		}

		public void MakeGameOverEffect () {

			Instantiate (this.gameoverEffect);

		}

	}
}
