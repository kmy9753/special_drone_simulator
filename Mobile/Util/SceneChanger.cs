﻿using UnityEngine;
using System.Collections;

public class SceneChanger : MonoBehaviour {

	[SerializeField] private float fadeTime;

	public void LoadLevel (string scene) {
		
		FadeManager.Instance.LoadLevel (scene, fadeTime);
		
	}

}
