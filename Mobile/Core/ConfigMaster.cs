﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ConfigMaster : SingletonMonoBehaviour<ConfigMaster> {

	[SerializeField] private Toggle checkBox_useSavedLocation;
	[SerializeField] private Toggle checkBox_withoutGPS;

	[SerializeField] private Toggle checkBox_time1;
	[SerializeField] private Toggle checkBox_time2;
	[SerializeField] private Toggle checkBox_time3;

	[SerializeField] private int [] timeLimits;

	private bool useSavedLocation;
	private bool playWithoutGPS;
	private int  timeLimit;

	void OnLevelWasLoaded () {

		this.timeLimits [0] = 30;
		this.timeLimits [1] = 60;
		this.timeLimits [2] = 120;

		GameObject obj = GameObject.Find ("UseSavedLocation");
		if (obj != null) {
			this.checkBox_useSavedLocation = obj.GetComponent<Toggle> ();
			this.checkBox_useSavedLocation.isOn = this.useSavedLocation;
		}

		obj = GameObject.Find ("WithoutGPS");
		if (obj != null) {
			this.checkBox_withoutGPS = obj.GetComponent<Toggle> ();
			this.checkBox_withoutGPS.isOn = this.playWithoutGPS;
		}

		obj = GameObject.Find ("Time1");
		if (obj != null) {
			this.checkBox_time1 = obj.GetComponent<Toggle> ();
			if (this.timeLimit == this.timeLimits [0]) this.checkBox_time1.isOn = true;
		}

		obj = GameObject.Find ("Time2");
		if (obj != null) {
			this.checkBox_time2 = obj.GetComponent<Toggle> ();
			if (this.timeLimit == this.timeLimits [1]) this.checkBox_time2.isOn = true;
		}
		obj = GameObject.Find ("Time3");
		if (obj != null) {
			this.checkBox_time3 = obj.GetComponent<Toggle> ();
			if (this.timeLimit == this.timeLimits [2]) this.checkBox_time3.isOn = true;
		}

	}

	public void Awake ()
	{
		if (this != Instance) {
			Destroy (this);
			return;
		}

		DontDestroyOnLoad (this.gameObject);

	}

	// Update is called once per frame
	void Update () {

		if (SceneManager.GetActiveScene ().name == "Title") {
			SetUseSavedLocation ();
			SetWithoutGPS ();
			SetTime ();
		}

	}

	public bool UseSavedLocation () {

		return this.useSavedLocation;

	}
	public bool PlayWithoutGPS () {

		return this.playWithoutGPS;

	}
	public int GetTimeLimit () {

		return this.timeLimit;

	}

	public void SetUseSavedLocation () {

		this.useSavedLocation = this.checkBox_useSavedLocation.isOn;

	}
	public void SetWithoutGPS () {

		this.playWithoutGPS = this.checkBox_withoutGPS.isOn;

	}
	public void SetTime () {

		if (this.checkBox_time1.isOn) this.timeLimit = this.timeLimits[0];
		if (this.checkBox_time2.isOn) this.timeLimit = this.timeLimits[1];
		if (this.checkBox_time3.isOn) this.timeLimit = this.timeLimits[2];

	}

}
