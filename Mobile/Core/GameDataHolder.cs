﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameDataHolder : SingletonMonoBehaviour<GameDataHolder> {

	private string pre_sceneName;
	private string cur_sceneName = "####";

	// Game Scene
	public int num_avoidance;
	public int remainingTime;

	public void Awake ()
	{
		if (this != Instance) {
			Destroy (this);
			return;
		}

		DontDestroyOnLoad (this.gameObject);

	}

	void OnLevelWasLoaded () {

		this.pre_sceneName = this.cur_sceneName;
		this.cur_sceneName = SceneManager.GetActiveScene ().name;

	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
