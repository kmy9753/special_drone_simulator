﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DualDecomposition;

using Game;

public class GameController : MonoBehaviour {

	[SerializeField] private Transform player;

	[SerializeField] private SceneChanger sceneChanger;

	[SerializeField] private string resultSceneName;

	[SerializeField] private Text timeTextField;
	[SerializeField] private Text avoidanceTextField;
	[SerializeField] private LocationTutor locationTutor;

	[SerializeField] private CenterScript centerScript;
	[SerializeField] private GameObject joystickObj;

	void Start () {

		GameDataHolder.Instance.remainingTime = ConfigMaster.Instance.GetTimeLimit ();
		GameDataHolder.Instance.num_avoidance = 0;
		this.timeTextField.text = GameDataHolder.Instance.remainingTime.ToString ();
		this.timeTextField.enabled = false;

		if (!ConfigMaster.Instance.PlayWithoutGPS ()) {
			this.joystickObj.SetActive (false);
		}

	}

	void CountDownTime () {

		GameDataHolder.Instance.remainingTime -= 1;
		this.timeTextField.text = GameDataHolder.Instance.remainingTime.ToString ();

		if (GameDataHolder.Instance.remainingTime <= 0) {
			CancelInvoke ("CountDownTime");

			EffectRenderer.Instance.MakeClearEffect ();
			Invoke ("TransitionToResultScene", 2f);
		}

	}

	void UpdateTextField () {

		this.avoidanceTextField.text = "ナイス回避：" + GameDataHolder.Instance.num_avoidance.ToString("D3") + "回";

	}

	public void FinishGame () {

		CancelInvoke ("CountDownTime");

		EffectRenderer.Instance.MakeGameoverEffect ();
		Invoke ("TransitionToResultScene", 2f);

	}

	void TransitionToResultScene () {

		this.sceneChanger.LoadLevel (this.resultSceneName);

	}

	void OnTriggerEnter (Collider collider) {

		if (collider.tag == "Player") {

			EffectRenderer.Instance.MakeGameStartEffect ();

			InvokeRepeating ("CountDownTime", 1f, 1f);
			InvokeRepeating ("UpdateTextField", 1f, 1f);

			this.GetComponent<Collider> ().enabled = false;
			this.GetComponent<MeshRenderer> ().enabled = false;
			this.locationTutor.DestroyObj ();

			this.timeTextField.enabled = true;

			this.centerScript.Activate ();
	
		}

	}

}
