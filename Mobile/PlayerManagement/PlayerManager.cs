﻿using UnityEngine;
using System.Collections;

using Game;

public class PlayerManager : MonoBehaviour {

	[SerializeField] GameController gameController;
	[SerializeField] Transform floor;

	private Vector2 min_position, max_position;

	void Start () {

		this.min_position = new Vector2 (-this.floor.lossyScale.x / 2f, -this.floor.lossyScale.z / 2f);
		this.max_position = new Vector2 ( this.floor.lossyScale.x / 2f,  this.floor.lossyScale.z / 2f);

	}

	// Update is called once per frame
	void Update () {

		if (this.transform.position.x < this.min_position.x || this.max_position.x < this.transform.position.x ||
		    this.transform.position.z < this.min_position.y || this.max_position.y < this.transform.position.z) {

			this.transform.position = new Vector3(Mathf.Clamp (this.transform.position.x, this.min_position.x, this.max_position.x),
			                                      this.transform.position.y,
			                                      Mathf.Clamp (this.transform.position.z, this.min_position.y, this.max_position.y));
		}

	}

	void OnCollisionEnter (Collision collision) {

		if (collision.gameObject.tag == "Enemy") {
			Vector3 hitPosition = this.transform.position;
			EffectRenderer.Instance.MakeTouchEffect (hitPosition);

			this.gameController.FinishGame ();
		}

	}


}
