﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // unit test (change bordertime with slider), (view location)

public class PlayerControllerWithGPS : MonoBehaviour {

	[SerializeField] private LocationTutor locationTutor;
	[SerializeField] private GameObject floor;
	private GPSManager gpsManager;

	private LocationConverter locationConverter;
	[SerializeField] private float borderTime;

	private Transform player;

	private bool isMoving;
	private Vector3 pre_position;
	public Vector2 speed; // [pixel/s]

	[SerializeField] private PlayerControllerWithSensors playerController_sensors;

	/* unit test (change bordertime with slider)
	[SerializeField] private Slider slider;
	[SerializeField] private Text text;
	//*/

	/* unit test (view location) (1/2)
	[SerializeField] private Text text_view_location;
	//*/

	//* unit test (view accuracy) (1/2)
	[SerializeField] private Text text_view_accuracy;
	//*/

	void Start () {
		
		this.player = this.gameObject.transform;
		this.gpsManager = GPSManager.Instance;
		this.locationConverter = LocationConverter.Instance;
		this.player.gameObject.GetComponent<Renderer> ().enabled = false;
		this.player.gameObject.GetComponent<Collider> ().enabled = false;

		this.isMoving = false;
		this.speed = new Vector2();

	}

	public void Activate (Transform startPoint) {

		this.player.position = startPoint.position;
		this.player.gameObject.GetComponent<Renderer> ().enabled = true;
		this.player.gameObject.GetComponent<Collider> ().enabled = true;

		this.pre_position = this.player.position;

		if (!ConfigMaster.Instance.PlayWithoutGPS ()) {
			InvokeRepeating ("UpdatePosition", this.borderTime, this.borderTime);
		}
	}
	
	void UpdatePosition () {

		/* unit test (change bordertime with slider)
		if (this.borderTime != slider.value) {
			this.borderTime = slider.value;
			this.text.text = "GPS取得間隔： " + this.borderTime.ToString ("F3") + " [s]";
			CancelInvoke    ("UpdatePosition");
			InvokeRepeating ("UpdatePosition", this.borderTime, this.borderTime);
		}
		//*/

		this.gpsManager.SensingLocation ();
		_Vector2 location = this.gpsManager.GetLocation ();

		/* unit test (view location) (2/2)
		this.text_view_location.text = location.x.ToString("F7") + ", " + location.y.ToString("F7");
		//*/

		//* unit test (view accuracy) (2/2)
		this.text_view_accuracy.text = this.gpsManager.GetAccuracy().ToString("F7");
		//*/

		Vector2 nextPosition2D = this.locationConverter.ConvertToVirtual (location);
		Vector3 nextPosition = new Vector3 (nextPosition2D.x, this.player.position.y, nextPosition2D.y);

		this.speed = new Vector2 (nextPosition.x - this.pre_position.x, nextPosition.z - this.pre_position.z) / this.borderTime;
		this.pre_position = nextPosition;


		this.isMoving = true;
		iTween.MoveTo(this.player.gameObject, iTween.Hash(
			"position", nextPosition,
			"time", this.borderTime * 0.2f, 
			"oncomplete", "CompleteMovement",
			"oncompletetarget", this.gameObject, 
			"easeType", iTween.EaseType.linear
		));

	}

	void CompleteMovement () {

		this.isMoving = false;
		// this.playerController_sensors.SetVelocity_mps (this.speed * 2.5f);
		this.playerController_sensors.SetVelocity_mps (this.speed * 3f);

	}

	public bool IsMoving () {

		return this.isMoving;

	}

}
