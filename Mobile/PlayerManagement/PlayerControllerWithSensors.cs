﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI; // unit test (change bordertime with slider), (view location)

public class PlayerControllerWithSensors : MonoBehaviour {

	[SerializeField] private Transform floor;

	[SerializeField] private float borderTime;

	/* unit test (change bordertime with slider) (1/2)
	[SerializeField] private Slider slider;
	[SerializeField] private Text text;
	//*/

	private Transform player;

	// private float verticalAngle_deg;

	private Vector2 velocity;
	// private Vector2 accelerometer;

	[SerializeField] private PlayerControllerWithGPS playerController_gps;

	void Start () {

		this.velocity		 = new Vector2 ();
		// this.accelerometer		 = new Vector2 ();
		// this.verticalAngle_deg = 0f;

		this.player = this.gameObject.transform;

	}

	public void Resume () {

		this.Pause ();
		InvokeRepeating ("UpdatePosition", this.borderTime, this.borderTime);

	}

	public void Pause () {

		CancelInvoke ("UpdatePosition");

	}

	public void Activate () {

		//* run without Sensors - (1/3)
		// this.verticalAngle_deg = LocationHolder.Instance.GetVerticalAngle_deg ();
		this.Resume();
		//*/
	}

	void UpdatePosition () {

		/* unit test (change bordertime with slider) (2/2)
		if (this.borderTime != slider.value) {
			this.borderTime = slider.value;
			this.text.text = "GPS取得間隔： " + this.borderTime.ToString ("F3") + " [s]";
			CancelInvoke    ("UpdatePosition");
			InvokeRepeating ("UpdatePosition", this.borderTime, this.borderTime);
		}
		//*/
		/*
		float angle_rad = (OrientationManager.Instance.GetOrientation () - this.verticalAngle_deg) * -1f / 180f * Mathf.PI;
		Vector3 cur_accelerometer_raw = AccelerometerManager.Instance.GetAccelerometer ();
		cur_accelerometer_raw *= -1f;

		Vector2 pre_a = this.accelerometer;
		Vector2 cur_a = new Vector2 (cur_accelerometer_raw.y,
			                	     0f);

		cur_a.y = cur_accelerometer_raw.x * -1f;
		*/
/*
		if (cur_accelerometer_raw.x * cur_accelerometer_raw.z >= 0f) {
			cur_a.y = Mathf.Sqrt (Mathf.Pow (cur_accelerometer_raw.x, 2f) + Mathf.Pow (cur_accelerometer_raw.z, 2f))
					* Mathf.Sign (cur_accelerometer_raw.x);
		} else {
			float x_abs = Mathf.Abs (cur_accelerometer_raw.x);
			float z_abs = Mathf.Abs (cur_accelerometer_raw.z);

			cur_a.y = (x_abs > z_abs) ? cur_accelerometer_raw.x : cur_accelerometer_raw.z;
		}
		cur_a.y *= -1;
*/
		/*
		// 加速度をorientationで座標変換
		cur_a = new Vector2(cur_a.x * Mathf.Cos(angle_rad) - cur_a.y * Mathf.Sin(angle_rad),
							cur_a.x * Mathf.Sin(angle_rad) + cur_a.y * Mathf.Cos(angle_rad));

		Vector2 pre_v_mps = this.velocity_mps;
		Vector2 cur_v_mps = new Vector2 (pre_v_mps.x + (pre_a.x + cur_a.x) / 2f * this.borderTime,
			                    pre_v_mps.y + (pre_a.y + cur_a.y) / 2f * this.borderTime);
		*/

		if (!this.playerController_gps.IsMoving ()) {
			/*
			Vector2 delta_m = new Vector2 ((pre_v_mps.x + cur_v_mps.x) / 2f * this.borderTime,
				(pre_v_mps.y + cur_v_mps.y) / 2f * this.borderTime);

			Vector3 delta = new Vector3 (delta_m.x * this.floor.lossyScale.x / LocationConverter.Instance.GetRealScale().x,
				0f,
				delta_m.y * this.floor.lossyScale.z / LocationConverter.Instance.GetRealScale().y);
			*/

			// float delta_scolor = Mathf.Sqrt(Mathf.Pow(this.playerController_gps.speed.x, 2f) + Mathf.Pow(this.playerController_gps.speed.y, 2f)) * this.borderTime;
			// Vector3 delta = new Vector3 (-delta_scolor * Mathf.Sin(angle_rad), 0f, delta_scolor * Mathf.Cos(angle_rad)) * 1f;

			// Vector3 delta = new Vector3 (velocity_mps.x * this.borderTime * this.floor.lossyScale.x / LocationConverter.Instance.GetRealScale().x, 0f,
			//	velocity_mps.y * this.borderTime * this.floor.lossyScale.y / LocationConverter.Instance.GetRealScale().y);

			Vector3	delta = new Vector3 (velocity.x * this.borderTime, 0f, velocity.y * this.borderTime);

			iTween.MoveTo(this.player.gameObject, iTween.Hash(
				"position", this.player.position + delta,
				"time", this.borderTime, 
				"easeType", iTween.EaseType.linear
			));

			this.velocity *= 0.9f;

		}
/*
		this.accelerometer = cur_a;
		this.velocity_mps  = cur_v_mps;
*/
	}

	public void SetVelocity_mps(Vector2 velocity){

		// this.velocity_mps = new Vector2 (velocity.x * LocationConverter.Instance.GetRealScale ().x / floor.lossyScale.x,
		//	velocity.y * LocationConverter.Instance.GetRealScale ().y / floor.lossyScale.y);

		this.velocity = velocity;

	}
}
