﻿using UnityEngine;
using System.Collections;
using UnitySampleAssets.CrossPlatformInput;

public class PlayerControllerWithJoyStick : MonoBehaviour {

	[SerializeField] private float speed;

	// Update is called once per frame
	void Update () {

		var hAxis = CrossPlatformInputManager.GetAxis("Horizontal");
		this.transform.position += Vector3.right * hAxis * speed * Time.deltaTime;

		var vAxis = CrossPlatformInputManager.GetAxis ("Vertical");
		this.transform.position += Vector3.forward * vAxis * speed * Time.deltaTime;

	}

}
