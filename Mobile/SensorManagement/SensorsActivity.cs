﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SensorsActivity : SingletonMonoBehaviour<SensorsActivity> {

	public AndroidJavaClass activity;

	void Start () {

		if (!ConfigMaster.Instance.PlayWithoutGPS ()) {
			AndroidJNI.AttachCurrentThread();
			this.activity = new AndroidJavaClass("com.kmy9753.SpecialOnigokko.SensorsActivity");
		}

	}

}
