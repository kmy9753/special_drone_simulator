﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/** Accessing the GPS Data over JNI calls
 *  Please check the Java sources under Plugins/Android
 * */
public class GPSManager : SingletonMonoBehaviour<GPSManager> {
	
	static string message;

	private double longitude;
	private double latitude;
	private double accuracy;
	private float  velocity;

	private Vector2 borderDelta;
	[SerializeField] private float borderTime;

	private bool hasCorrectLocation;
	private int countContinuousCorrectLocation;

	void Start () {

		this.hasCorrectLocation = false;
		this.countContinuousCorrectLocation = 0;

		// todo: it's an experimental data (maybe not correct)
		this.borderDelta = new Vector2 ((float)1e-4, (float)1e-4);

		InvokeRepeating ("SensingLocation", 0f, this.borderTime);

	}

	public void ResetHasCorrectLocation () {

		this.hasCorrectLocation = false;
		this.countContinuousCorrectLocation = 0;

	}
	
	public void SensingLocation () {
		
		if (ConfigMaster.Instance.PlayWithoutGPS ()) {
			this.longitude = 0;
			this.latitude  = 0;
			this.hasCorrectLocation = true;
			return;
		}

		double cur_longitude = 0f;
		double cur_latitude  = 0f;

		//GPS & wi-fiで取得した緯度経度を変数に代入
		message = SensorsActivity.Instance.activity.CallStatic<string>("getLongitude");
		if(message != "Unknown") {
			cur_longitude = double.Parse(message);
		}
		
		message = SensorsActivity.Instance.activity.CallStatic<string>("getLatitude");
		if(message != "Unknown") {
			cur_latitude = double.Parse(message);
		}

		Vector2 delta = new Vector2(Mathf.Abs((float)(cur_latitude  - latitude )),
									Mathf.Abs((float)(cur_longitude - longitude)));
		
		if (delta.x > borderDelta.x ||
		    delta.y > borderDelta.y) {
			if (!this.hasCorrectLocation) {
				this.latitude  = cur_latitude;
				this.longitude = cur_longitude;
				this.accuracy  = double.Parse(SensorsActivity.Instance.activity.CallStatic<string>("getAccuracy"));
				this.velocity  = float.Parse(SensorsActivity.Instance.activity.CallStatic<string>("getSpeed"));

				this.countContinuousCorrectLocation = 0;
			}
		} else {
			this.latitude  = cur_latitude;
			this.longitude = cur_longitude;
			this.accuracy  = double.Parse(SensorsActivity.Instance.activity.CallStatic<string>("getAccuracy"));
			this.velocity  = float.Parse(SensorsActivity.Instance.activity.CallStatic<string>("getSpeed"));

			if(!this.hasCorrectLocation) {
				if(++countContinuousCorrectLocation >= 5) {
					hasCorrectLocation = true;
				}
			}
		}
	}
	
	public double GetLongitude () {
		
		return this.longitude;
		
	}
	public double GetLatitude () {
		
		return this.latitude;
		
	}

	// return (longitude, latitude)
	public _Vector2 GetLocation () {
		
		return new _Vector2 (this.longitude, this.latitude);

	}

	public double GetAccuracy () {

		return this.accuracy;

	}

	public float GetSpeed () {

		return this.velocity;

	}

	public bool HasCorrectLocation () {

		return this.hasCorrectLocation;

	}

}
