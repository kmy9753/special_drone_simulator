﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AccelerometerManager : SingletonMonoBehaviour<AccelerometerManager> {

	static string message;

	/* unit test (view accelermeter) (1/3)
	[SerializeField] private Text viewText_x;
	[SerializeField] private Text viewText_y;
	[SerializeField] private Text viewText_z;
	//*/

	void Start () {

		/* unit test (view accelermeter) (2/3)
		InvokeRepeating("GetAccelerometer", 1f, 0.05f);
		//*/

	}

	public Vector3 GetAccelerometer () {

		Vector3 accelerometer = new Vector3(float.Parse(SensorsActivity.Instance.activity.CallStatic<string>("getAccelerometerX")),
											float.Parse(SensorsActivity.Instance.activity.CallStatic<string>("getAccelerometerY")),
											float.Parse(SensorsActivity.Instance.activity.CallStatic<string>("getAccelerometerZ")));

		/* unit test (view accelermeter) (3/3)
		viewText_x.text = accelerometer.x.ToString ("F5");
		viewText_y.text = accelerometer.y.ToString ("F5");
		viewText_z.text = accelerometer.z.ToString ("F5");
		//*/

		return accelerometer;

	}

	/*
	void Pause () {
		GPSManager.Instance.gpsActivityJavaClass.CallStatic<string>("pause");
	}

	void Resume () {
		GPSManager.Instance.gpsActivityJavaClass.CallStatic<string>("resume");
	}
	*/

}
