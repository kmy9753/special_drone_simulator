﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

/** Accessing the GPS Data over JNI calls
 *  Please check the Java sources under Plugins/Android
 * */
public class OrientationManager : SingletonMonoBehaviour<OrientationManager> {

	static string message;

	/* unit test (view orientation) (1/3)
	[SerializeField] private Text viewText;
	//*/

	void Start () {

		/* unit test (view orientation) (2/3)
		InvokeRepeating("GetOrientation", 1f, 0.05f);
		//*/

	}

	public int GetOrientation () {

		int orientation = 0;

		message = SensorsActivity.Instance.activity.CallStatic<string>("getAzimuth");
		if(message != "Unknown") {
			orientation = int.Parse(message);
		}

		/* unit test (view orientation) (3/3)
		viewText.text = orientation.ToString();
		//*/

		return orientation;

	}

	/*
	void Pause () {
		GPSManager.Instance.gpsActivityJavaClass.CallStatic<string>("pause");
	}

	void Resume () {
		GPSManager.Instance.gpsActivityJavaClass.CallStatic<string>("resume");
	}
	*/

}
