﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public class PincerSubSystem : SubSystem {

		private PincerMasterSystem pMasterSystem;

		public void SetMasterSystem (PincerMasterSystem masterSystem) {

			this.masterSystem = masterSystem;

		}

		protected override void SolverMethod (float [][] rambda) {

			int numSS = this.masterSystem.GetNumSubSystem();

			this.omega = 0f;

			this.omega -= 0.5f * rambda[0][this.index];

			if(this.index % 2 == 0 && this.index <= numSS - 2) {
				this.omega += 0.5f * rambda[0][this.index + 2];
			}

			if(this.index == 0){
				this.omega += 0.5f * rambda[0][this.index + 1];
			}

			this.velocity = 0f;
			if(this.index != numSS - 1) {
				this.velocity += 0.5f * rambda[1][this.index+1];
			}
			this.velocity -= 0.5f * rambda[1][this.index];

		}

		protected override void GradientMethod (float [][] rambda) {
		}
	}

}
