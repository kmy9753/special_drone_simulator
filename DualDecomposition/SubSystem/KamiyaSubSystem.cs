﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public class KamiyaSubSystem : SubSystem {
		
		private KamiyaMasterSystem kMasterSystem;
		
		public void SetMasterSystem (KamiyaMasterSystem masterSystem) {

			this.masterSystem = masterSystem;
			
		}
		
		protected override void SolverMethod (float [][] rambda) {
		
			int numSS = this.masterSystem.GetNumSubSystem();

			this.omega = 0f;

			this.omega -= 0.5f * rambda[0][this.index];
			
			if(this.index != numSS - 1) {
				this.omega += 0.5f * rambda[0][this.index + 1];
			}
			
			this.velocity = 0f;
			if(this.index != numSS - 1) {
				this.velocity += 0.5f * rambda[1][this.index+1];
			}
			this.velocity -= 0.5f * rambda[1][this.index];
		
		}
		
		protected override void GradientMethod (float [][] rambda) {
		}
	}

}