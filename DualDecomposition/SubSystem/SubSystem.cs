﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public abstract class SubSystem : MonoBehaviour {
	
		protected MasterSystem masterSystem;
		protected float theta;
		protected int index;
		protected float omega;
		protected float velocity;
		protected float radius;
		
		protected bool active;
		
		public virtual void UpdateSystem (float [][] rambda) {
		
			SolverMethod (rambda);
			
			this.omega = Mathf.Clamp(this.omega, -0.5f, 0.5f);
			this.velocity = Mathf.Clamp(this.velocity, -2f, 2f);
			
			this.theta += this.omega;
			this.theta += 2f * Mathf.PI * 100f;
			this.theta %= (2f * Mathf.PI);
			this.radius += this.velocity;
			
		}
		
		/* update formula is old */
		protected abstract void SolverMethod (float [][] rambda);
		
		protected abstract void GradientMethod (float [][] rambda);
		
		public void SetActive () {
		
			this.active = true;
		
		}
		public void UnsetActive () {
		
			this.active = false;
		
		}
		public bool IsActive () {
		
			return this.active;
		
		}
		
		public void SetMasterSystem (MasterSystem masterSystem) {
		
			this.masterSystem = masterSystem;
		
		}
		public void SetTheta (float theta) {
		
			this.theta = theta;
		
		}
		public void SetIndex (int index) {
			
			this.index = index;
			
		}
		public void SetOmega (float omega) {
		
			this.omega = omega;
		
		}
		public void SetVelocity (float velocity) {
		
			this.velocity = velocity;
		
		}
		public void SetCenter (Vector3 center) {
		
			float x = this.transform.position.x - center.x;
			float z = this.transform.position.z - center.z;
			
			this.radius = Mathf.Sqrt(Mathf.Pow(x, 2f) + Mathf.Pow(z, 2f));
			this.theta = Mathf.Atan(z / x);
			if(x < 0f) {
				this.theta += Mathf.PI;
			}
		
		}
		
		public float GetTheta () {
		
			return this.theta;
		
		}
		public float GetRadius () {
		
			return this.radius;
		
		}
		public float GetBorderTIme () {
		
			return this.masterSystem.GetBorderTime();
		
		}
		
		public Vector3 GetEuclideanPosition () {
		
			Vector3 position = this.masterSystem.GetCenter();
			
			position.x += this.radius * Mathf.Cos(this.theta);
			position.z += this.radius * Mathf.Sin(this.theta);
			
			return position;
			
		}
		
	}

}
