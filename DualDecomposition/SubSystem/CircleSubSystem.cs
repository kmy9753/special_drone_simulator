﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public class CircleSubSystem : SubSystem {
		
		private CircleMasterSystem cMasterSystem;
		
		public void SetMasterSystem (CircleMasterSystem masterSystem) {
			
			this.masterSystem = masterSystem;
			this.cMasterSystem = masterSystem;
			
		}
		
		protected override void SolverMethod (float [][] rambda) {
		
			int numSS = this.masterSystem.GetNumSubSystem();
			int k = this.cMasterSystem.GetIntervalIndex();
			
			this.omega = this.cMasterSystem.GetOmegaStar();
			if(this.index != 0) {
				this.omega -= 0.5f * rambda[0][this.index - 1];
			}
			if(this.index != numSS - k) {
				this.omega += 0.5f * rambda[0][(this.index - 1 + k) % numSS];
			}
			
			this.velocity = 0f;
			if(this.index == 0) {
				this.velocity -= 0.5f * rambda[2][0];
			}
			else {
				this.velocity -= 0.5f * rambda[1][this.index - 1];
			}
			if(this.index != numSS - k) {
				this.velocity += 0.5f * rambda[1][(this.index - 1 + k) % numSS];
			}
		
		}
		
		protected override void GradientMethod (float [][] rambda) {
		}
	}

}