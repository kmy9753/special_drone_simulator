﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {
	
	public class TigerSubSystem : SubSystem {
		
		// private TigerMasterSystem tMasterSystem;
		
		public void SetMasterSystem (TigerMasterSystem masterSystem) {
			
			this.masterSystem = masterSystem;
			// this.tMasterSystem = masterSystem;
			
		}
		
		protected override void SolverMethod (float [][] rambda) {
			
			int numSS = this.masterSystem.GetNumSubSystem();
			
			this.omega = 0f;
			this.omega -= 0.5f * rambda[0][this.index];
			if(this.index != numSS - 1) {
				this.omega += 0.5f * rambda[0][this.index + 1];
			}
			
			this.velocity = 0f;
			this.velocity -= 0.5f * rambda[1][this.index];
			if(this.index != numSS - 1) {
				this.velocity += 0.5f * rambda[1][this.index + 1];
			}
			
		}
		
		protected override void GradientMethod (float [][] rambda) {
		}
	}
	
}