﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public class DroneBeheivier : MonoBehaviour {
	
		[SerializeField] private Transform base_;
		private SubSystem subSystem;
	
		private float intervalTime;
		private float borderTime;
	
		public Vector3 distination;
	
		// Use this for initialization
		void Start () {
		
			this.intervalTime = 0f;
			
		}
		
		// Update is called once per frame
		void Update () {
			
			if(this.subSystem == null) {
				this.subSystem = this.gameObject.GetComponent<SubSystem>();
			}
			if(this.subSystem == null) return;
			
			this.borderTime = this.subSystem.GetBorderTIme();
			this.intervalTime += Time.deltaTime;
			
			if(this.intervalTime > this.borderTime) {
				
				if(this.subSystem.IsActive()) {
					this.distination = this.subSystem.GetEuclideanPosition();
				}
				else {
					this.distination = this.base_.position;
				}
			
				iTween.MoveTo(this.gameObject, iTween.Hash(
					"position", this.distination,
					"time", borderTime,
					"oncomplete", "OnComplete", 
					"oncompletetarget", this.gameObject, 
					"easeType", iTween.EaseType.easeOutSine
					//"space", Space.worldでグローバル座標系で移動
					));
				
				intervalTime = 0f;
			}
			
		}
		
		public void OnComplete() {
			
			iTween.Stop(this.gameObject, "move");
			
		}
	}

}