﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {
	
	public class Edge1MasterSystem : MasterSystem {

		[SerializeField] private float radiusStar;
		[SerializeField] private int Status;
		private Edge1SubSystem [] e1SubSystem;
		
		private float omegaStar;
		
		// Use this for initialization
		void Start () {
			
			this.active = false;
			
			this.e1SubSystem = new Edge1SubSystem[this.kMax_numSS];
			this.subSystem = new SubSystem[this.kMax_numSS];
			
			omegaStar = 0f;
			
			this.rambda = new float[2][];
			rambda[0] = new float[kMax_numSS];
			rambda[1] = new float[kMax_numSS];
			
		}
		
		// Update is called once per frame
		void Update () {
			
			this.UpdateSystem();
			
		}
		
		protected override void InitSubsystem (Transform ss, int index) {
			
			this.subSystem[index] = this.e1SubSystem[index] = ss.gameObject.AddComponent<Edge1SubSystem>();
			
			this.e1SubSystem[index].SetIndex(index);
			this.e1SubSystem[index].SetMasterSystem(this);
			
			this.subSystem[index] = this.e1SubSystem[index];
			
		}
		
		protected override void InitRambda () {
			
			for(int i = 0; i < this.kMax_numSS - 1; i++) {
				this.rambda[0][i] = Random.Range(this.omegaStar / 2f, this.omegaStar / 2f);
				this.rambda[1][i] = Random.Range(-this.radiusStar / 2f, this.radiusStar / 2f);
			}
			
		}
		
		
		protected override void GradientMethod (float [] theta, float [] radius) {
			float theta1;
			float theta2;
			if (this.numSS % 2 == 0) {
				theta1 = Mathf.PI / 2f / ((int)(this.numSS + 2));
				theta2 = Mathf.PI / 2f / ((int)(this.numSS + 1));
			} else {
				theta1 = Mathf.PI / 2f / ((int)(this.numSS + 1));
				theta2 = theta1;
			}
			//Debug.Log("On");
			for(int i = 0; i < this.numSS; i++) {
				
				// about theta
				if(i == 0){
					this.rambda[0][i] = (theta[i] - Mathf.PI /4 - this.Status * Mathf.PI/2);
				}else if(i == 1){
					this.rambda[0][i] = (theta[i] - theta[i-1] - theta1);
				}else if(i % 2 == 0){
					this.rambda[0][i] = (theta[i] - theta[i-2] + theta2);
				}else{
					this.rambda[0][i] = (theta[i] - theta[i-2] - theta1);
				}
				
				this.rambda[0][i] += 2f * Mathf.PI * 100f;
				this.rambda[0][i] %= 2f * Mathf.PI;
				if(this.rambda[0][i] > Mathf.PI) this.rambda[0][i] -= 2f * Mathf.PI;
				this.rambda[0][i] *= alpha;
				
				// about radius
				if(i == 0){
					this.rambda[1][i] = alpha * (radius[i] - this.radiusStar);
				}
				else{
					this.rambda[1][i] = alpha * (radius[i] - radius[i -1]);
				}
			}
			
		}
		
		public void IncreaseRadiusStar () {
			
			this.radiusStar += 1f;
			
		}
		public void DecreaseRadiusStar () {
			
			if(this.radiusStar - 1f > 0f + 1e-5) this.radiusStar -= 3f;
			
		}
		public void SetRadiusStar (float radiusStar) {
			
			this.radiusStar = radiusStar;
			
		}
		
		public float GetRadiusStar () {
			
			return this.radiusStar;
			
		}

		public void SetStatus (int S){
			this.Status = S;
		}
		
	}
	
}
