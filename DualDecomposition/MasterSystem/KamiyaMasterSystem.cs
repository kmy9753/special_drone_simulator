﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public class KamiyaMasterSystem : MasterSystem {
	
		[SerializeField] private float w;
		[SerializeField] private float radiusStar;
		private KamiyaSubSystem [] cSubSystem;

		private float omegaStar;
		private float thetaStar;
		private float radiusDelta;
		private float time;
		
		// Use this for initialization
		void Start () {
		
			this.active = false;
			
			this.cSubSystem = new KamiyaSubSystem[this.kMax_numSS];
			this.subSystem = new SubSystem[this.kMax_numSS];

			omegaStar = 0f;
			
			this.rambda = new float[2][];
			rambda[0] = new float[kMax_numSS-1];
			rambda[1] = new float[kMax_numSS];

		}
		
		// Update is called once per frame
		void Update () {
			
			this.UpdateSystem();
			
		}
		
		protected override void InitSubsystem (Transform ss, int index) {
			
			this.subSystem[index] = this.cSubSystem[index] = ss.gameObject.AddComponent<KamiyaSubSystem>();
			
			this.cSubSystem[index].SetIndex(index);
			this.cSubSystem[index].SetMasterSystem(this);
			
			this.subSystem[index] = this.cSubSystem[index];
		
		}
		
		protected override void InitRambda () {
					
			for(int i = 0; i < this.kMax_numSS - 1; i++) {
				this.rambda[0][i] = Random.Range(this.omegaStar / 2f, this.omegaStar / 2f);
				this.rambda[1][i] = Random.Range(-this.radiusStar / 2f, this.radiusStar / 2f);
			}

		}
		
				
		protected override void GradientMethod (float [] theta, float [] radius) {
			time += this.intervalTime;
			thetaStar = Mathf.PI / this.numSS * Mathf.Sin(this.w * time);
			radiusDelta = this.radiusStar / Mathf.Abs(Mathf.Cos(thetaStar));
			//Debug.Log("On");
			for(int i = 0; i < this.numSS; i++) {
				
				// about theta
				if(i == 0){
					this.rambda[0][i] = (theta[i] - thetaStar);
				}
				else{
					this.rambda[0][i] = (theta[i] - theta[i-1] - 2f * Mathf.PI / this.numSS);
				}
				
				this.rambda[0][i] += 2f * Mathf.PI * 100f;
				this.rambda[0][i] %= 2f * Mathf.PI;
				if(this.rambda[0][i] > Mathf.PI) this.rambda[0][i] -= 2f * Mathf.PI;
				this.rambda[0][i] *= alpha;
				
				// about radius
				if(i == 0){
					this.rambda[1][i] = alpha * (radius[i] - radiusDelta);
				}
				else{
					this.rambda[1][i] = alpha * (radius[i] - radius[i -1]);
				}
			}
			
		}

		public void IncreaseRadiusStar () {
		
			this.radiusStar += 1f;
		
		}
		public void DecreaseRadiusStar () {
			
			if(this.radiusStar - 1f > 0f + 1e-5) this.radiusStar -= 1.5f;
			
		}
		public void SetRadiusStar (float radiusStar) {
			
			this.radiusStar = radiusStar;
		
		}

		public float GetRadiusStar () {
		
			return this.radiusStar;
		
		}
		
	}

}
