﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public class CircleMasterSystem : MasterSystem {
	
		[SerializeField] private float omegaStar;
		[SerializeField] private float radiusStar;
		private CircleSubSystem [] cSubSystem;
		
		private int [] intervalIndex;
		
		// Use this for initialization
		void Start () {
		
			this.active = false;
			
			this.cSubSystem = new CircleSubSystem[this.kMax_numSS];
			this.subSystem = new SubSystem[this.kMax_numSS];
			
			this.intervalIndex = new int[this.kMax_numSS + 1];
			this.InitIntervalIndex();
			
			this.rambda = new float[3][];
			rambda[0] = new float[kMax_numSS];
			rambda[1] = new float[kMax_numSS];
			rambda[2] = new float[1];
			
		}
		
		// Update is called once per frame
		void Update () {
			
			this.UpdateSystem();
			
		}
		
		protected override void InitSubsystem (Transform ss, int index) {
			
			this.subSystem[index] = this.cSubSystem[index] = ss.gameObject.AddComponent<CircleSubSystem>();
			
			this.cSubSystem[index].SetIndex(index);
			this.cSubSystem[index].SetMasterSystem(this);
			
			this.subSystem[index] = this.cSubSystem[index];
		
		}
		
		protected override void InitRambda () {
					
			for(int i = 0; i < this.kMax_numSS - 1; i++) {
				this.rambda[0][i] = Random.Range(this.omegaStar / 2f, this.omegaStar / 2f);
				this.rambda[1][i] = Random.Range(-this.radiusStar / 2f, this.radiusStar / 2f);
			}
			
			this.rambda[2][0] = Random.Range(-this.radiusStar / 2f, this.radiusStar / 2f);

		}
		
		private void InitIntervalIndex () {
		
			for(int i = 0; i < this.intervalIndex.Length; i++) {
				this.intervalIndex[i] = 1;
			}
			
			/*
			this.intervalIndex[5]  = 2;
			this.intervalIndex[7]  = 3;
			this.intervalIndex[8]  = 3;
			this.intervalIndex[9]  = 4;
			this.intervalIndex[10] = 3;
			*/
			
		}
				
		protected override void GradientMethod (float [] theta, float [] radius) {
			
			for(int i = 0; i < this.numSS - 1; i++) {
				
				int k = this.intervalIndex[this.numSS];
				
				// about theta
				this.rambda[0][i] = (theta[(i + 1) % this.numSS] - theta[(i + 1 - k + this.numSS) % this.numSS] - (2f * Mathf.PI / this.numSS) * k);
				
				this.rambda[0][i] += 2f * Mathf.PI * 100f;
				this.rambda[0][i] %= 2f * Mathf.PI;
				if(this.rambda[0][i] > Mathf.PI) this.rambda[0][i] -= 2f * Mathf.PI;
				this.rambda[0][i] *= alpha;
				
				// about radius
				this.rambda[1][i] = alpha * (radius[(i + 1) % this.numSS] - radius[(i + 1 - k + this.numSS) % this.numSS]);
			}
			
			this.rambda[2][0] = alpha * (radius[0] - this.radiusStar);
			
		}

		public void IncreaseRadiusStar () {
		
			if(!this.IsActive()) return;
			this.radiusStar += 1f;
		
		}
		public void DecreaseRadiusStar () {
			
			if(!this.IsActive()) return;
			if(this.radiusStar - 1f > 0f + 1e-5) this.radiusStar -= 1f;
			
		}
		public void SetRadiusStar (float radiusStar) {
			
			this.radiusStar = radiusStar;
		
		}
		
		public int GetIntervalIndex () {
		
			return this.intervalIndex[this.numSS];
		
		}
		public float GetOmegaStar () {
		
			return this.omegaStar;
		
		}
		public float GetRadiusStar () {
		
			return this.radiusStar;
		
		}
		
	}

}