﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {
	
	public abstract class MasterSystem : MonoBehaviour {
		
		[SerializeField] protected int kMax_numSS;
		[SerializeField] protected int kMin_numSS;
		[SerializeField] protected int numSS;
		[SerializeField] protected Transform center;
		[SerializeField] protected float alpha;
		
		protected float [][] rambda;
		protected float intervalTime;
		[SerializeField] protected float borderTime;
		protected SubSystem [] subSystem;
		[SerializeField] protected Transform droneRoot;
		
		protected bool active;
		
		protected int delta_numSS;
	
		// Update is called once per frame
		protected void UpdateSystem () {
			
			if(!this.active) return;
			
			this.intervalTime += Time.deltaTime;
			
			if(this.intervalTime > this.borderTime) {
				this.ApplyNumSSChange();
				
				float [] theta = new float[this.numSS];
				float [] radius = new float[this.numSS];
				
				for(int i = 0; i < this.numSS; i++) {
					this.subSystem[i].UpdateSystem(this.rambda);
					theta[i] = this.subSystem[i].GetTheta();
					radius[i] = this.subSystem[i].GetRadius();
				}
				
				this.GradientMethod(theta, radius);
				
				this.intervalTime = 0f;
				
			}
			
		}
		
		protected abstract void GradientMethod (float [] theta, float [] radius);
		protected abstract void InitRambda ();
		protected abstract void InitSubsystem (Transform ss, int index);
		
		public void SetActive () {
			
			if(this.active) return;
			this.active = true;
			
			this.intervalTime = 0f;
			this.delta_numSS = 0;
			
			this.InitRambda();
			
			int index = 0;
			foreach(Transform ss in droneRoot) {
				if(index < kMax_numSS) {
					this.InitSubsystem(ss, index);
				}
				index++;
			}
			this.kMax_numSS = Mathf.Min(this.kMax_numSS, index);
			this.numSS = Mathf.Min(this.numSS, this.kMax_numSS);
			
			for(int i = 0; i < this.numSS; i++) {
				this.ActivateSubSystem(i);
			}
			
		}
		
		public void UnsetActive () {
		
			if(!this.active) return;
			this.active = false;
			
			int index = 0;
			foreach(Transform ss in droneRoot) {
				if(index < kMax_numSS) {
					Object.Destroy(ss.GetComponent<SubSystem>());
				}
				index++;
			}
		
		}
		
		protected void ActivateSubSystem (int index) {
			
			this.subSystem[index].SetCenter(this.center.position);
			this.subSystem[index].SetActive();
			
		}
		
		public void IncreaseNumSubSystem () {
			
			if(!this.IsActive()) return;
			if(this.numSS + delta_numSS == this.kMax_numSS) {
				return;
			}
			
			this.delta_numSS++;
			
		}
		public void DecreaseNumSubSystem () {
			
			if(!this.IsActive()) return;
			if(this.numSS + delta_numSS == kMin_numSS) {
				return;
			}
			
			this.delta_numSS--;
			
		}
		
		public void IncreaseAlpha () {
			
			if(!this.IsActive()) return;
			if(this.alpha + 0.1f <= 1.0f + 1e-5) this.alpha += 0.1f;
			
		}
		public void DecreaseAlpha () {
			
			if(!this.IsActive()) return;
			if(this.alpha - 0.1f >= 0.1f - 1e-5) this.alpha -= 0.1f;
			
		}
		
		public void ApplyNumSSChange () {
			
			if(this.delta_numSS > 0) {
				for(int i = this.numSS; i < this.numSS + this.delta_numSS; i++) {
					this.ActivateSubSystem(i);
				}
			}
			else if(this.delta_numSS < 0) {
				for(int i = this.numSS - 1; i >= this.numSS + this.delta_numSS; i--) {
					this.subSystem[i].UnsetActive();
				}
			}
			
			this.numSS += this.delta_numSS;
			this.delta_numSS = 0;
			
		}

		public int GetNumSubSystem () {
			
			return this.numSS;
			
		}
		public Vector3 GetCenter () {
			
			return this.center.position;
			
		}

		public float GetAlpha () {
			
			return this.alpha;
			
		}
		
		public float GetBorderTime () {
		
			return this.borderTime;
		
		}
		
		public bool IsActive () {
		
			return this.active;
		
		}
		
	}
	
}