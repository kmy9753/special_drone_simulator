﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

	public class PincerMasterSystem : MasterSystem {

		[SerializeField] private float radiusStar;
		[SerializeField] private float distance;
		private PincerSubSystem [] pSubSystem;

		private float omegaStar;
		private float thetaStar;
		private float time;

		private float D1;
		private int n1;

		// Use this for initialization
		void Start () {

			this.active = false;

			this.pSubSystem = new PincerSubSystem[this.kMax_numSS];
			this.subSystem = new SubSystem[this.kMax_numSS];

			omegaStar = 0f;
			thetaStar = Random.Range(-Mathf.PI, Mathf.PI);
			time = 0f;

			this.rambda = new float[2][];
			rambda[0] = new float[kMax_numSS];
			rambda[1] = new float[kMax_numSS];

		}

		// Update is called once per frame
		void Update () {

			this.UpdateSystem();

		}

		protected override void InitSubsystem (Transform ss, int index) {

			this.subSystem[index] = this.pSubSystem[index] = ss.gameObject.AddComponent<PincerSubSystem>();

			this.pSubSystem[index].SetIndex(index);
			this.pSubSystem[index].SetMasterSystem(this);

			this.subSystem[index] = this.pSubSystem[index];

		}

		protected override void InitRambda () {

			for(int i = 0; i < this.kMax_numSS; i++) {
				this.rambda[0][i] = Random.Range(this.omegaStar / 2f, this.omegaStar / 2f);
				this.rambda[1][i] = Random.Range(-this.radiusStar / 2f, this.radiusStar / 2f);
			}

		}


		protected override void GradientMethod (float [] theta, float [] radius) {
			time += 1f;
			thetaStar += Mathf.PI / 4f * Mathf.Sin (time);

			if(this.numSS % 2 == 0){
				n1 = (int)(this.numSS) / 2 - 1;
				D1 = n1 * distance;
			}else{
				n1 = (this.numSS + 1) / 2 - 1;
				D1 = n1 * distance;
			}

			float [] displ;
			displ = new float[n1];

			float D1s = D1/2f;
			float D1g = D1s;

			for (int i = 0; i < n1; i++) {
				if (D1s == 0f) {
				}
				else if(D1s < distance) {
					displ [i] = Mathf.Atan (D1s / radiusStar) * 2f;
				} else {
					displ [i] = Mathf.Atan (D1s / radiusStar) - Mathf.Atan ((D1s - distance) / radiusStar); 
				}

				D1g -= distance; 
				D1s = Mathf.Abs(D1g);
			}



			int j = 0;
			//Debug.Log("On");
			for(int i = 0; i < this.numSS; i++) {

				// about theta
				if(i == 0){
					this.rambda[0][i] = (theta[i] - thetaStar);
				}
				else if(i%2 == 0){
					this.rambda [0] [i] = theta [i] - (theta [i - 2] + displ [j]);
					j++;
				}else{
					this.rambda[0][i] = (theta[i] - theta[i-1] - Mathf.PI);
				}

				this.rambda[0][i] += 2f * Mathf.PI * 100f;
				this.rambda[0][i] %= 2f * Mathf.PI;
				if(this.rambda[0][i] > Mathf.PI) this.rambda[0][i] -= 2f * Mathf.PI;
				this.rambda[0][i] *= alpha;

				// about radius
				if(i == 0){
					this.rambda[1][i] = alpha * (radius[i] - radiusStar);
				}
				else{
					this.rambda[1][i] = alpha * (radius[i] - radius[i -1]);
				}
			}

		}

		public void IncreaseRadiusStar () {

			this.radiusStar += 1f;

		}
		public void DecreaseRadiusStar () {

			if(this.radiusStar - 1f > 0f + 1e-5) this.radiusStar -= 4f;

		}
		public void SetRadiusStar (float radiusStar) {

			this.radiusStar = radiusStar;

		}

		public float GetRadiusStar () {

			return this.radiusStar;

		}

	}

}