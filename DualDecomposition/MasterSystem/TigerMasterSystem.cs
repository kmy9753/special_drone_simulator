﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {
	
	public class TigerMasterSystem : MasterSystem {
		
		private float omegaStar;
		[SerializeField] private float radiusStar;
		[SerializeField] private int numPartition;
		[SerializeField] private float thetaZeroStar;
		private TigerSubSystem [] tSubSystem;
		
		// Use this for initialization
		void Start () {
			
			this.active = false;
			omegaStar = 0f;
			
			this.tSubSystem = new TigerSubSystem[this.kMax_numSS];
			this.subSystem = new SubSystem[this.kMax_numSS];
			
			this.rambda = new float[2][];
			rambda[0] = new float[kMax_numSS];
			rambda[1] = new float[kMax_numSS];
			
			this.SetActive();
			
		}
		
		// Update is called once per frame
		void Update () {
			
			this.thetaZeroStar = ((180f - this.center.transform.eulerAngles.y) / 180f) * Mathf.PI;
			this.UpdateSystem();
			
		}
		
		protected override void InitSubsystem (Transform ss, int index) {
			
			this.subSystem[index] = this.tSubSystem[index] = ss.gameObject.AddComponent<TigerSubSystem>();
			
			this.tSubSystem[index].SetIndex(index);
			this.tSubSystem[index].SetMasterSystem(this);
			
			this.subSystem[index] = this.tSubSystem[index];
			
		}
		
		protected override void InitRambda () {
			
			for(int i = 0; i < this.kMax_numSS - 1; i++) {
				this.rambda[0][i] = Random.Range(this.omegaStar / 2f, this.omegaStar / 2f);
				this.rambda[1][i] = Random.Range(-this.radiusStar / 2f, this.radiusStar / 2f);
			}
			
		}
		
		protected override void GradientMethod (float [] theta, float [] radius) {
			
			for(int i = 0; i < this.numSS; i++) {
				
				// about theta
				if(i == 0){
					this.rambda[0][i] = theta[i] - this.thetaZeroStar;
				}
				else{
					this.rambda[0][i] = (theta[i] - theta[i - 1] - 2f * Mathf.PI / this.numPartition);
				}
				
				this.rambda[0][i] += 2f * Mathf.PI * 100f;
				this.rambda[0][i] %= 2f * Mathf.PI;
				if(this.rambda[0][i] > Mathf.PI) this.rambda[0][i] -= 2f * Mathf.PI;
				this.rambda[0][i] *= alpha;
				
				// about radius
				if(i == 0){
					this.rambda[1][i] = alpha * (radius[i] );
				}
				else if(i % this.numPartition == 1 || this.numPartition == 1){
					this.rambda[1][i] = alpha * (radius[i] - radius[i - 1]- this.radiusStar);
				}
				else{
					this.rambda[1][i] = alpha * (radius[i] - radius[i -1]);
				}
			}
			
		}
		
		public void IncreaseRadiusStar () {
			
			if(!this.IsActive()) return;
			this.radiusStar += 1f;
			
		}
		public void DecreaseRadiusStar () {
			
			if(!this.IsActive()) return;
			if(this.radiusStar - 1f > 0f + 1e-5) this.radiusStar -= 1f;
			
		}
		
		public float GetOmegaStar () {
			
			return this.omegaStar;
			
		}
		public float GetRadiusStar () {
			
			return this.radiusStar;
			
		}
		
	}
	
}