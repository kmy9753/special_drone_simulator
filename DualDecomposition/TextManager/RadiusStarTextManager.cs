﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DualDecomposition;

public class RadiusStarTextManager : MonoBehaviour {

	private Text textField;
	[SerializeField] private CircleMasterSystem cMasterSystem;
	[SerializeField] private TigerMasterSystem tMasterSystem;

	// Use this for initialization
	void Start () {
	
		textField = this.GetComponent<Text>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		if(this.cMasterSystem.IsActive()) {
			textField.text = "→ RadiusStar: " + this.cMasterSystem.GetRadiusStar().ToString("F1");
		}
		
		if(this.tMasterSystem.IsActive()) {
			textField.text = "→ RadiusStar: " + this.tMasterSystem.GetRadiusStar().ToString("F1");
		}
		
	}
}
