﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DualDecomposition;

public class NumSSTextManager : MonoBehaviour {

	private Text textField;
	[SerializeField] private Transform masterSystemRoot;
	private MasterSystem [] masterSystem;

	// Use this for initialization
	void Start () {
	
		textField = this.GetComponent<Text>();
		
		this.masterSystem = new MasterSystem[this.masterSystemRoot.childCount];
		int index = 0;
		foreach(Transform ms in this.masterSystemRoot) {
			this.masterSystem[index] = ms.GetComponent<MasterSystem>();
			index++;
		}
		
	}
	
	// Update is called once per frame
	void Update () {
		
		for(int i = 0; i < this.masterSystem.Length; i++) {
			if(this.masterSystem[i] != null && this.masterSystem[i].IsActive()) {
				textField.text = "→ Sub System: " + this.masterSystem[i].GetNumSubSystem();
			}
		}
		
	}
}
