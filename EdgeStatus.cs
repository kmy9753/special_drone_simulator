﻿using UnityEngine;
using System.Collections;

namespace DualDecomposition {

public class EdgeStatus : MonoBehaviour {

	[SerializeField] private CenterScript centerS;
	[SerializeField] private int Status;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider collider) {
		
		if(collider.transform.tag == "Player") {
				centerS.EdgeOnTriggerEnter(this.Status,this.transform.position);
		}
		
	}
	
	void OnTriggerExit (Collider collider) {
		
		if(collider.transform.tag == "Player") {
				centerS.SendMessage("EdgeOnTriggerExit");
			
		}
		
	}
}
}
