﻿using UnityEngine;
using System.Collections;

namespace Simple {
	
	public class DroneBeheivier : MonoBehaviour {
	
		[SerializeField] private Transform targetObject;
	
		private float time;
	
		// Use this for initialization
		void Start () {
		
			time = 0f;
			
		}
		
		// Update is called once per frame
		void Update () {
		
			time += Time.deltaTime;
			
			if(time > 2f){
				iTween.MoveTo(this.gameObject, iTween.Hash(
					"position", targetObject,
					"time", 0.9f, 
					"oncomplete", "complete", 
					"oncompletetarget", this.gameObject, 
					"easeType", "linear"
					//"space", Space.worldでグローバル座標系で移動
					));
				
				iTween.RotateTo(this.gameObject, iTween.Hash(
					"rotation", targetObject,
					"time", 0.9f
					));
					
				time = 0f;
			}
				
		}
		
		public void complete() {
		
			iTween.Stop(this.gameObject, "move");
			
		}
	}

}
