﻿using UnityEngine;
using System.Collections;

namespace Simple {
	
	public class LeaderController : MonoBehaviour {
	
		private float time;
		[SerializeField] private Transform floor;
		private Vector3 lowerSide;
		private Vector3 upperSide;
		
		// Use this for initialization
		void Start () {
			
			time = 0f;
			lowerSide = new Vector3(floor.position.x - floor.lossyScale.x / 2f, 0f, floor.position.z - floor.lossyScale.z / 2f);
			upperSide = new Vector3(floor.position.x + floor.lossyScale.x / 2f, 0f, floor.position.z + floor.lossyScale.z / 2f);
			
		}
		
		// Update is called once per frame
		void Update () {
			
			time += Time.deltaTime;
			
			if(time > 2f){
			
				Vector3 translate_delta = new Vector3(Random.Range(-5f, 5f), 0f, Random.Range(-5f, 5f));
				this.transform.Translate(translate_delta);						
				
				float rotation_dy = Random.Range(-Mathf.PI, Mathf.PI);
				this.transform.Rotate(new Vector3(0f, rotation_dy, 0f));
				
				// adjust position under x = [l, u], z = [l, u]
				transform.position = new Vector3(Mathf.Clamp(transform.position.x, lowerSide.x, upperSide.x),
											     transform.position.y,
				                                 Mathf.Clamp(transform.position.z, lowerSide.z, upperSide.z));
				
				time = 0f;
			}
			
		}
		
	}

}
